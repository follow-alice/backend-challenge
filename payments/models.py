from django.db import models
from django.utils.translation import gettext_lazy as _
from djmoney.models.fields import MoneyField
from moneyed import USD


class Payment(models.Model):
    class PaymentStatus(models.TextChoices):
        PENDING = "PENDING", _("Pending")
        PARTIAL = "PARTIAL", _("Partial")
        COMPLETED = "COMPLETED", _("Completed")

    id = models.BigAutoField(primary_key=True)
    amount_due = MoneyField(
        max_digits=14,
        decimal_places=2,
        default_currency=USD,
        currency_choices=[(USD.code, USD.get_name("en_US"))],
        verbose_name="Payment Amount Due",
    )
    amount_received = MoneyField(
        max_digits=14,
        decimal_places=2,
        default_currency=USD,
        currency_choices=[(USD.code, USD.get_name("en_US"))],
        verbose_name=("Payment Amount Received",),
    )
    status = models.CharField(
        max_length=10,
        choices=PaymentStatus.choices,
        default=PaymentStatus.PENDING,
        verbose_name="Payment Status",
    )
    invoice = models.ForeignKey(
        "invoices.Invoice", on_delete=models.PROTECT, related_name="payments"
    )

    due = models.DateField(verbose_name="Due date")
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
