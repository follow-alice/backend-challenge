from datetime import timedelta

from django.test import TestCase
from django.utils import timezone
from djmoney.money import Money

from invoices.models import Invoice, Client
from payments.models import Payment


class InvoicesViewTestCase(TestCase):
    def setUp(self):
        self.client_instance = Client.objects.create(
            first_name="John", last_name="Doe", email="test@test.com"
        )

        self.invoice = Invoice.objects.create(
            client=self.client_instance,
            name="Test Invoice",
            status=Invoice.Status.PARTIAL,
            trip_date=timezone.now() + timedelta(days=180),
        )

        self.deposit = Payment.objects.create(
            invoice=self.invoice,
            amount_due=Money(100, "USD"),
            amount_received=Money(100, "USD"),
            due=timezone.now(),
        )

        self.remaining_payment = Payment.objects.create(
            invoice=self.invoice,
            amount_due=Money(1000, "USD"),
            amount_received=Money(0, "USD"),
            due=timezone.now() + timedelta(days=30),
        )

    def test_invoices_view(self):
        response = self.client.get("/api/invoices/invoices/")

        total_due = (self.deposit.amount_due + self.remaining_payment.amount_due).amount
        total_received = (
            self.deposit.amount_received + self.remaining_payment.amount_received
        ).amount
        expected_data = {
            "id": self.invoice.id,
            "name": self.invoice.name,
            "client_email": self.client_instance.email,
            "status": self.invoice.status.value,
            "trip_date": self.invoice.trip_date.date().isoformat(),
            "total_due": f"{total_due:.2f}",
            "total_received": f"{total_received:.2f}",
        }

        self.assertEqual(200, response.status_code)
        self.assertEqual(
            {
                "count": 1,
                "next": None,
                "previous": None,
                "results": [expected_data],
            },
            response.json(),
        )

    def test_invoice_status_filter(self):
        self.invoice = Invoice.objects.create(
            client=self.client_instance,
            name="Test Invoice",
            status=Invoice.Status.PENDING,
            trip_date=timezone.now() + timedelta(days=180),
        )
        self.assertEqual(2, Invoice.objects.count())
        response = self.client.get("/api/invoices/invoices/", {"status": "PARTIAL"})
        self.assertEqual(1, len(response.json()["results"]))

    def test_invoice_status_update(self):
        self.remaining_payment.amount_received = self.remaining_payment.amount_due
        self.remaining_payment.status = Payment.PaymentStatus.COMPLETED
        self.remaining_payment.save()

        self.invoice.refresh_from_db()
        self.assertEqual(Invoice.Status.PAID, self.invoice.status)
