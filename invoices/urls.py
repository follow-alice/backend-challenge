from django.urls import path, include
from rest_framework import routers

from invoices import views

router = routers.SimpleRouter()
router.register(r"invoices", views.InvoiceViewSet)

# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
urlpatterns = router.urls
