import uuid

from django.db import models
from django.utils.translation import gettext_lazy as _


class Client(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    first_name = models.CharField(max_length=256, blank=True, null=True)
    last_name = models.CharField(max_length=256, blank=True, null=True)
    email = models.EmailField(unique=True, blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)


class Invoice(models.Model):
    class Status(models.TextChoices):
        PENDING = "PENDING", _("Pending")
        PARTIAL = "PARTIAL", _("Partial")
        PAID = "PAID", _("Paid")
        COMPLETED = "COMPLETED", _("Completed")
        ARCHIVED = "ARCHIVED", _("Archived")
        RESCHEDULED = "RESCHEDULED", _("Rescheduled")
        CANCELLED = "CANCELLED", _("Cancelled")

    id = models.BigAutoField(primary_key=True)
    trip_date = models.DateField(db_index=True)
    name = models.CharField(max_length=128, verbose_name="Invoice Name")
    status = models.CharField(
        max_length=32,
        choices=Status.choices,
        default=Status.PENDING,
        verbose_name="Invoice Status",
    )
    client = models.ForeignKey(
        "invoices.Client", on_delete=models.PROTECT, related_name="lead_invoices"
    )

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
