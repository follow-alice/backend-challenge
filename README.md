## Welcome to the Follow Alice backend challenge!

**Required tools:**
- Python 3.10
- poetry

**Install all dependencies:**
```bash
poetry install --no-root
```

**Run tests:**
```bash
poetry run ./manage.py test
```

## Challenges:

1. Please add the logic to the invoice list view that uses the following JSON format:
    ```json
    [{
        "id": "invoice id",
        "name": "invoice name",
        "client_email": "client name",
        "status": "invoice status",
        "trip_date": "invoice trip date",
        "total_due": "Sum of all due payments",
        "total_received": "Sum of all received payments"
    }, ...]
    ```

2. Add pagination and filtering by invoice status to the endpoint.

3. Add the necessary business logic that when all the payments are received, the invoice status changes to "PAID".

4. Please elaborate in a few sentences, what your thoughts on signals in Django are. 